﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleDestroy : MonoBehaviour
{
    private ParticleSystem particleSystem;
    
    private void Awake()
    {
        particleSystem = GetComponent<ParticleSystem>();
        particleSystem.Play();
        StartCoroutine(ParticleDestroyCoroutine(particleSystem.main.startLifetime.constant));
    }

    private IEnumerator ParticleDestroyCoroutine(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
