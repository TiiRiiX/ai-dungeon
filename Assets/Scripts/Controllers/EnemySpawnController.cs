﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawnController : MonoBehaviour
{
    public static EnemySpawnController Instance = null;
    
    [SerializeField] private GameObject[] enemyVariants;
    [SerializeField] private Transform[] spawnPoints;
    [SerializeField] private int maxEnemyOnLevel;
    [SerializeField] private float maxEnemyComplicationOnLevel;
    [SerializeField] private float spawnDelay;
    [SerializeField] private float spawnDelayRandomDelta;
    [SerializeField] private float spawnDelayComplicationOnLevel;

    private float nextTimeSpawned = 0f;
    private List<GameObject> enemies = new List<GameObject>();

    private int maxEnemy =>
        maxEnemyOnLevel + Mathf.RoundToInt(maxEnemyComplicationOnLevel * LevelController.Instance.CurrentLevel); 

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }

        Instance = this;
    }

    private void Start()
    {
        SetNextTimeSpawned();
    }

    private void Update()
    {
        if (enemyVariants.Length == 0 || spawnPoints.Length == 0 || LevelController.Instance.IsLevelFinish) return;
        if (Time.time > nextTimeSpawned && enemies.Count < maxEnemy)
        {
            enemies.Add(Instantiate(enemyVariants[Random.Range(0, enemyVariants.Length)],
                spawnPoints[Random.Range(0, spawnPoints.Length)].position, Quaternion.identity));
            SetNextTimeSpawned();
        }
    }

    public void AfterDead(GameObject enemy)
    {
        enemies.Remove(enemy);
        SetNextTimeSpawned();
    }

    private void SetNextTimeSpawned()
    {
        var spawnDelayWithComplication =
            Mathf.Clamp(spawnDelay - spawnDelayComplicationOnLevel * LevelController.Instance.CurrentLevel, 0.1f,
                spawnDelay);
        nextTimeSpawned = Time.time + spawnDelayWithComplication + Random.Range(0, spawnDelayRandomDelta);
    }

    public void LevelStart()
    {
        SetNextTimeSpawned();
    }

    public void LevelFinish()
    {
        for (var i = enemies.Count - 1; i >= 0; i--)
        {
            enemies[i].GetComponent<Unit>().Dead();
        }
    }
}
