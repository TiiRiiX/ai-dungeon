﻿using TMPro;
using UnityEngine;

public class LevelUIController : MonoBehaviour
{
    public static LevelUIController Instance = null;
    
    [SerializeField] private TMP_Text finalText;
    [SerializeField] private TMP_Text currentLevelText;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }

        Instance = this;
    }

    public void SetFinalText(string text)
    {
        finalText.SetText(text);
    }
    
    public void SetCurrentLevelText(int level)
    {
        currentLevelText.SetText($"Current level: {level}");    
    }
}
