﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance = null;
    public int CurrentLevel { private set; get; } = 0;

    [SerializeField] private GameObject[] heroesPrefabs;
    [SerializeField] private float delayLevelReload;
    [SerializeField] private GameObject cameraPrefab;

    private List<Hero> heroes = new List<Hero>();
    private Transform finishTransform;
    public bool IsLevelFinish { private set; get; } = false;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }

        Instance = this;
        finishTransform = GameObject.FindWithTag("Finish").transform;
    }

    private void Start()
    {
        StartLevel();
    }

    private void FixedUpdate()
    {
        if (heroes.Count > 0)
        {
            var closestHero = heroes.OrderBy(hero => Vector3.Distance(hero.transform.position, finishTransform.position)).First();
            if (closestHero.FollowCamera.gameObject.activeSelf) return;
            closestHero.FollowCamera.gameObject.SetActive(true);
            foreach (var hero in heroes.Where(hero => hero != closestHero))
            {
                hero.FollowCamera.gameObject.SetActive(false);
            }
        }
    }

    private void StartLevel()
    {
        var oldHeroes = FindObjectsOfType<Hero>();
        foreach (var hero in oldHeroes)
        {
            Destroy(hero.FollowCamera.gameObject);
            Destroy(hero.gameObject);
        }
        heroes.Clear();
        foreach (var heroPrefab in heroesPrefabs)
        {
            var heroObject = Instantiate(heroPrefab, Vector3.zero, Quaternion.identity);
            var hero = heroObject.GetComponent<Hero>();
            var cameraObject = Instantiate(cameraPrefab, Vector3.zero, Quaternion.identity);
            var camera = cameraObject.GetComponent<CinemachineVirtualCamera>();
            hero.FollowCamera = camera;
            camera.Follow = hero.transform;
            camera.LookAt = hero.transform;
            camera.gameObject.SetActive(false);
        }
        LevelUIController.Instance.SetFinalText(string.Empty);
        LevelUIController.Instance.SetCurrentLevelText(CurrentLevel + 1);
        IsLevelFinish = false;
        EnemySpawnController.Instance.LevelStart();
    }

    public void NewHeroInit(Hero hero)
    {
        heroes.Add(hero);
    }

    public void HeroDead(Hero hero)
    {
        heroes.Remove(hero);
        if (heroes.Count == 0)
        {
            LevelUIController.Instance.SetFinalText("You lose :(");
            EnemySpawnController.Instance.LevelFinish();
            IsLevelFinish = true;
            StartCoroutine(RestartLevelCoroutine(delayLevelReload));
        }
    }

    public void Win()
    {
        if (IsLevelFinish) return;
        IsLevelFinish = true;
        LevelUIController.Instance.SetFinalText("You win!");
        EnemySpawnController.Instance.LevelFinish();
        foreach (var hero in heroes)
        {
            hero.MoveController.SetStopped(true);
        }

        CurrentLevel++;
        StartCoroutine(RestartLevelCoroutine(delayLevelReload));
    }

    private IEnumerator RestartLevelCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        StartLevel();
    }
}
