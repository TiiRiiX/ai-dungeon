﻿public class Enemy : Unit
{
    public override void Dead()
    {
        EnemySpawnController.Instance.AfterDead(gameObject);
        base.Dead();
    }
}
