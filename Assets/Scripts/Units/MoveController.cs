﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class MoveController : MonoBehaviour
{
    public NavMeshAgent Agent { get; private set; }

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
    }

    public void Init(float speed)
    {
        Agent.speed = speed;
    }

    public void SetDestination(Vector3 target)
    {
        Agent.SetDestination(target);
    } 

    public void SetStopped(bool isStopped)
    {
        Agent.isStopped = isStopped;
    }
}