﻿using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(MoveController))]
public class Hero : Unit
{
    [HideInInspector] public CinemachineVirtualCamera FollowCamera;
    
    protected override void Start()
    {
        transform.position = GameObject.FindWithTag("Start").transform.position;
        LevelController.Instance.NewHeroInit(this);
        base.Start();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Finish"))
        {
            LevelController.Instance.Win();
        }
    }

    public override void Dead()
    {
        LevelController.Instance.HeroDead(this);
        base.Dead();
    }

    public void Heal(float healPercent)
    {
        var newCurrentHp = currentHp + Hp * healPercent;
        currentHp = Mathf.Clamp(newCurrentHp, 0, Hp);
    }
}