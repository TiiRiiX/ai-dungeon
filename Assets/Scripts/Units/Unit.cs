﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[RequireComponent(typeof(MoveController))]
public class Unit : MonoBehaviour
{
    [HideInInspector] public Transform BattleTarget;
    public MoveController MoveController { get; protected set; }

    public float AttackRadius => attackRadius;
    public State AttackState => attackStateInstance;
    public State MoveState => moveStateInstance;
    public State StartState => startStateInstance; 
    public float AttackDelay => attackDelay;
    public GameObject ParticlesAttack => particlesAttack;
    public GameObject ParticlesPower => particlesPower;
    public float Attack => attack + attackUpgrade * LevelController.Instance.CurrentLevel;
    public float Defence => defence + defenceUpgrade * LevelController.Instance.CurrentLevel;
    public float Speed => speed + speedUpgrade * LevelController.Instance.CurrentLevel;
    public float Hp => hp + hpUpgrade * LevelController.Instance.CurrentLevel;

    [Header("Parameters")] 
    [SerializeField] protected float attack;
    [SerializeField] protected float attackUpgrade;
    [SerializeField] protected float defence;
    [SerializeField] protected float defenceUpgrade;
    [SerializeField] protected float hp;
    [SerializeField] protected float hpUpgrade;
    [SerializeField] protected float speed;
    [SerializeField] protected float speedUpgrade;
    [SerializeField] protected float attackRadius;
    [SerializeField] protected float attackDelay;
    [SerializeField] protected float powerIncrease;

    [Header("States")] 
    [SerializeField] protected State startState;
    [SerializeField] protected State attackState;
    [SerializeField] protected State moveState;
    [SerializeField] protected State powerState;

    [Header("Others")] 
    [SerializeField] protected GameObject particlesAttack;
    [SerializeField] protected GameObject particlesPower;
    [SerializeField] protected Slider healthBar;
    [SerializeField] protected Slider powerBar;
    
    protected State currentState = null;
    protected float currentHp;
    protected State startStateInstance;
    protected State attackStateInstance;
    protected State moveStateInstance;
    protected State powerStateInstance;
    protected float power = 0f;

    protected virtual void Awake()
    {
        MoveController = GetComponent<MoveController>();
        startStateInstance = Instantiate(startState);
        attackStateInstance = Instantiate(attackState);
        moveStateInstance = Instantiate(moveState);
        if (powerState != null)
        {
            powerStateInstance = Instantiate(powerState);
        }
        else
        {
            powerBar.gameObject.SetActive(false);
        }

        healthBar.value = 1f;
        powerBar.value = 0f;
        currentHp = Hp;
    }

    protected virtual void Start()
    {
        MoveController.Init(Speed);
        SetState(startStateInstance);
    }

    protected void FixedUpdate()
    {
        if (currentState != null)
        {
            currentState.Run();
        }
    }

    public bool Damage(float damage)
    {
        var randomDamage = damage / 2 + Random.Range(0.1f, damage);
        currentHp -= Mathf.Clamp(randomDamage - Defence, 0.1f, randomDamage);
        var isAlive = currentHp > 0;
        healthBar.value = currentHp / Hp;
        AddPower();
        if (!isAlive)
        {
            Dead();
        }

        return isAlive;
    }

    public void AddPower()
    {
        power += powerIncrease;
        if (power >= 1)
        {
            RunPower();
            power = 0;
        }

        powerBar.value = power;
    }

    protected virtual void RunPower()
    {
        if (powerStateInstance != null)
        {
            powerStateInstance.Init(this);
            powerStateInstance.Run();
        }
    }

    public virtual void Dead()
    {
        if (currentState != null)
        {
            currentState.Stop();
        }

        currentState = null;
        Destroy(gameObject);
    }

    public void SetState(State state)
    {
        if (currentState != null)
        {
            currentState.Stop();
        }

        currentState = state;
        currentState.Init(this);
        Debug.Log($"{gameObject.name}: change to state {state.name}");
    }
}