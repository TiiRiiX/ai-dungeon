﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class MoveToFinishState : State
{
    private Transform targetTransform;
    
    public override void Init(Unit unit)
    {
        base.Init(unit);
        targetTransform = GameObject.FindWithTag("Finish").transform;
        Unit.MoveController.SetStopped(false);
        Unit.MoveController.SetDestination(targetTransform.position);
    }

    public override void Run()
    {
        var checkEnemyHit = CheckEnemyHit();
        if (checkEnemyHit != null)
        {
            Unit.BattleTarget = checkEnemyHit;
            Unit.SetState(Unit.AttackState);
        }
    }
    
    private Transform CheckEnemyHit()
    {
        var colliders = Physics.OverlapSphere(Unit.transform.position, Unit.AttackRadius);
        if (colliders.Length > 0)
        {
            var enemyCollider = colliders
                .Where(c => c.gameObject.GetComponent<Enemy>() != null)
                .OrderBy(c => Vector3.Distance(c.transform.position, Unit.transform.position)).FirstOrDefault();
            if (enemyCollider != null && (Unit.BattleTarget == null || enemyCollider.transform != Unit.BattleTarget))
            {
                var enemyTransform = enemyCollider.transform;
                RaycastHit hit;
                var raycastDirection = enemyTransform.position - Unit.transform.position;
                if (Physics.Raycast(Unit.transform.position, raycastDirection, out hit))
                {
                    if (hit.transform == enemyTransform)
                    {
                        return enemyTransform;
                    }
                }
            }
        }

        return null;
    }

    public override void Stop()
    {
        Unit.MoveController.SetStopped(true);
    }
}
