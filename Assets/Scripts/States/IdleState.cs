﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class IdleState : State
{
    public override void Init(Unit unit)
    {
        base.Init(unit);
    }
    
    public override void Run()
    {
        var players = FindObjectsOfType<Hero>();
        if (players.Length > 0)
        {
            var closestPlayer = players.OrderBy(hero => Vector3.Distance(hero.transform.position, Unit.transform.position)).First();
            Unit.BattleTarget = closestPlayer.transform;
            Unit.SetState(Unit.MoveState);
        }
    }

    public override void Stop()
    {
        
    }
    
}
