﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class PowerKillState : State
{
    public override void Init(Unit unit)
    {
        base.Init(unit);
    }
    
    public override void Run()
    {
        if (Unit.BattleTarget != null)
        {
            Unit.BattleTarget.GetComponent<Unit>().Dead();
            Unit.BattleTarget = null;
        }
        Instantiate(Unit.ParticlesPower, Unit.transform.position + Unit.transform.localScale / 2, Quaternion.identity);
        Unit.SetState(Unit.StartState);
    }

    public override void Stop()
    {
        
    }
    
}
