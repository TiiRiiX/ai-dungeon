﻿using UnityEngine;

[CreateAssetMenu]
public class AttackState : State
{
    [SerializeField] private State idleState;

    private float lastShootTime;
    private State idleStateInstance;
    
    public override void Init(Unit unit)
    {
        base.Init(unit);
        idleStateInstance = Instantiate(idleState);
    }

    public override void Run()
    {
        if (Unit.BattleTarget == null)
        {
            Unit.SetState(idleStateInstance);
            return;
        }
        var time = Time.time;
        if (lastShootTime + Unit.AttackDelay < time)
        {
            lastShootTime = time;
            Fire();
        }
    }

    private void Fire()
    {
        var isAlive = Unit.BattleTarget.GetComponent<Unit>().Damage(Unit.Attack);
        Instantiate(Unit.ParticlesAttack, Unit.BattleTarget.transform.position + Unit.BattleTarget.transform.localScale / 2, Quaternion.identity);
        if (!isAlive)
        {
            Unit.BattleTarget = null;
            Unit.SetState(idleStateInstance);
        }
        else
        {
            Unit.SetState(Unit.MoveState);   
        }
    }

    public override void Stop()
    {
        
    }
}
