﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class MoveToPlayerState : State
{
    [SerializeField] private State idleState;
    
    private Transform targetTransform = null;
    private State idleStateInstance;

    public override void Init(Unit unit)
    {
        base.Init(unit);
        var players = FindObjectsOfType<Hero>();
        if (players.Length > 0)
        {
            var closestPlayer = players.OrderBy(hero => Vector3.Distance(hero.transform.position, Unit.transform.position)).First();
            targetTransform = closestPlayer.transform;
            Unit.BattleTarget = targetTransform;
        }
        else
        {
            Unit.SetState(idleStateInstance);
        }
        idleStateInstance = Instantiate(idleState);
    }

    public override void Run()
    {
        if (targetTransform == null)
        {
            Unit.SetState(idleStateInstance);
            return;
        }
        Unit.MoveController.SetStopped(false);
        Unit.MoveController.SetDestination(targetTransform.position);
        RaycastHit hit;
        var raycastDirection = Unit.BattleTarget.position - Unit.transform.position;
        if (Physics.Raycast(Unit.transform.position, raycastDirection, out hit))
        {
            if (hit.transform == Unit.BattleTarget)
            {
                if (Vector3.Distance(Unit.transform.position, Unit.BattleTarget.position) < Unit.AttackRadius)
                {
                    Unit.SetState(Unit.AttackState);
                }
            }
        }
    }

    public override void Stop()
    {
        Unit.MoveController.SetStopped(true);
    }
}
