﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class MultiShotState : State
{
    [SerializeField] private int shotCount;
    
    public override void Init(Unit unit)
    {
        base.Init(unit);
    }
    
    public override void Run()
    {
        if (Unit.BattleTarget != null)
        {
            var unit = Unit.BattleTarget.GetComponent<Unit>();
            for (int i = 0; i < shotCount; i++)
            {
                var isAlive = unit.Damage(Unit.Attack);
                if (!isAlive)
                {
                    Unit.BattleTarget = null;
                    break;
                }
            }
        }
        Instantiate(Unit.ParticlesPower, Unit.transform.position + Unit.transform.localScale / 2, Quaternion.identity);
        Unit.SetState(Unit.StartState);
    }

    public override void Stop()
    {
        
    }
    
}
