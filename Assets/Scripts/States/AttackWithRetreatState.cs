﻿using UnityEngine;

[CreateAssetMenu]
public class AttackWithRetreatState : State
{
    private float lastShootTime;
    
    public override void Run()
    {
        if (Unit.BattleTarget == null)
        {
            Unit.SetState(Unit.MoveState);
            return;
        }
        
        if (Vector3.Distance(Unit.BattleTarget.transform.position ,Unit.transform.position) < Unit.AttackRadius / 2) {
            Vector3 targetPosition = (Unit.BattleTarget.transform.position - Unit.transform.position).normalized * -3f;
            Unit.MoveController.Agent.destination = targetPosition;
            Unit.MoveController.SetStopped(false);
        }
        else
        {
            Unit.MoveController.SetStopped(true);
        }
        
        var time = Time.time;
        if (lastShootTime + Unit.AttackDelay < time)
        {
            lastShootTime = time;
            Fire();
        }
    }
    
    private void Fire()
    {
        RaycastHit hit;
        var raycastDirection = Unit.BattleTarget.position - Unit.transform.position;
        if (Physics.Raycast(Unit.transform.position, raycastDirection, out hit))
        {
            var unit = hit.transform.gameObject.GetComponent<Unit>();
            if (unit != null && !unit.CompareTag(Unit.tag))
            {
                unit.Damage(Unit.Attack);
                Unit.AddPower();
                Instantiate(Unit.ParticlesAttack, unit.transform.position + unit.transform.localScale / 2, Quaternion.identity);
                Unit.SetState(Unit.MoveState);
                Unit.BattleTarget = null;
            }
        }
    }

    public override void Stop()
    {
        
    }
}
