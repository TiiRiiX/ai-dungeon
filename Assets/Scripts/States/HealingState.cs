﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class HealingState : State
{
    [Range(0f, 1f)] [SerializeField] private float healAmountPercent;
    
    public override void Init(Unit unit)
    {
        base.Init(unit);
    }
    
    public override void Run()
    {
        var heroes = FindObjectsOfType<Hero>();
        foreach (var hero in heroes)
        {
            hero.Heal(healAmountPercent);
        }
        Instantiate(Unit.ParticlesPower, Unit.transform.position + Unit.transform.localScale / 2, Quaternion.identity);
        Unit.SetState(Unit.StartState);
    }

    public override void Stop()
    {
        
    }
    
}
