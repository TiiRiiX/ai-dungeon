﻿using UnityEngine;

public abstract class State : ScriptableObject
{
    public Unit Unit { get; private set; }

    public virtual void Init(Unit unit)
    {
        Unit = unit;
    }
    public abstract void Run();
    public abstract void Stop();
}
